#include <stdio.h>
#include <time.h>
#include <unistd.h>

typedef struct 
{
    struct timespec start_time;
    struct timespec end_time;
} Timer;

void start_timer(Timer *timer) 
{
    clock_gettime(CLOCK_MONOTONIC, &timer->start_time);
}

void stop_timer(Timer *timer) 
{
    clock_gettime(CLOCK_MONOTONIC, &timer->end_time);
}

long get_elapsed_time_ns(Timer *timer) 
{
    return (timer->end_time.tv_sec - timer->start_time.tv_sec) * 1e9 +(timer->end_time.tv_nsec - timer->start_time.tv_nsec);
}

double compute_normalized_offset(long measured_time_ns, long expected_time_ns) 
{
    return 100.0 * (measured_time_ns - expected_time_ns) / expected_time_ns;
}

int main() 
{
    Timer timer;
    long sleep_time_ms = 100;
    long sleep_time_ns = sleep_time_ms * 1e6;
    start_timer(&timer);

    usleep(sleep_time_ms * 1000);


    stop_timer(&timer);

    long elapsed_time_ns = get_elapsed_time_ns(&timer);

    double offset = compute_normalized_offset(elapsed_time_ns, sleep_time_ns);

    printf("Expected sleep time: %ld ns\n", sleep_time_ns);
    printf("Measured sleep time: %ld ns\n", elapsed_time_ns);
    printf("Normalized offset: %f%%\n", offset);
    
    return 0;
}
