#include <stdio.h>
#include <time.h>

double sum_numbers(long n) 
{
    double sum = 0;
    for (long i = 1; i <= n; i++) 
    {
        sum += i;
    }
    return sum;
}
double multiply_numbers(long n) 
{
    double product = 1;
    for (long i = 1; i <= n; i++) 
    {
        product *= i;
    }
    return product;
}
int main() 
{
    clock_t start_time, end_time;
    double cpu_time_used;
    long n = 100000;
    start_time = clock();
    double sum_result = sum_numbers(n);
    end_time = clock();
    cpu_time_used = ((double) (end_time - start_time)) / CLOCKS_PER_SEC;
    printf("Sum result: %f\n", sum_result);
    printf("CPU time used for summing: %f seconds\n", cpu_time_used);
    start_time = clock();
    double multiply_result = multiply_numbers(n);
    end_time = clock();
    cpu_time_used = ((double) (end_time - start_time)) / CLOCKS_PER_SEC;
    printf("Multiply result: %f\n", multiply_result);
    printf("CPU time used for multiplying: %f seconds\n", cpu_time_used);
    return 0;
}
