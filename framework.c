#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct 
{
    clock_t start_time;
    clock_t end_time;
} Timer;

void start_timer(Timer *timer) 
{
    timer->start_time = clock();
}

void stop_timer(Timer *timer) 
{
    timer->end_time = clock();
}

double get_elapsed_time(Timer *timer) 
{
    return ((double)(timer->end_time - timer->start_time)) / CLOCKS_PER_SEC;
}

typedef struct 
{
    void (*log)(const char* message);
} Logger;

void console_log(const char* message) 
{
    printf("%s\n", message);
}
Logger ConsoleLogger = { console_log };

typedef struct 
{
    Logger base;
    FILE *file;
} FileLogger;

void file_log(const char* message) 
{
    FILE *file = fopen("log.txt", "a");
    if (file == NULL) {
        fprintf(stderr, "Error opening file for logging.\n");
        return;
    }
    fprintf(file, "%s\n", message);
    fclose(file);
}

FileLogger create_file_logger(const char *filename) 
{
    FileLogger logger;
    logger.base.log = file_log;
    logger.file = fopen(filename, "a");
    if (logger.file == NULL) {
        fprintf(stderr, "Error opening file: %s\n", filename);
        exit(1);
    }
    fclose(logger.file);
    return logger;
}

double sum_numbers(long n) 
{
    double sum = 0;
    for (long i = 1; i <= n; i++) 
    {
        sum += i;
    }
    return sum;
}

double multiply_numbers(long n) 
{
    double product = 1;
    for (long i = 1; i <= n; i++) 
    {
        product *= i;
    }
    return product;
}

int main() 
{
    Timer timer;
    long n = 10000; 

    
    FileLogger file_logger = create_file_logger("log.txt");

    
    start_timer(&timer);
    double sum_result = sum_numbers(n);
    stop_timer(&timer);
    double sum_time = get_elapsed_time(&timer);
    
    
    char buffer[100];
    sprintf(buffer, "Sum result: %f", sum_result);
    ConsoleLogger.log(buffer);
    file_logger.base.log(buffer);

    sprintf(buffer, "CPU time used for summing: %f seconds", sum_time);
    ConsoleLogger.log(buffer);
    file_logger.base.log(buffer);

    
    start_timer(&timer);
    double multiply_result = multiply_numbers(n);
    stop_timer(&timer);
    double multiply_time = get_elapsed_time(&timer);
    
    
    sprintf(buffer, "Multiply result: %f", multiply_result);
    ConsoleLogger.log(buffer);
    file_logger.base.log(buffer);

    sprintf(buffer, "CPU time used for multiplying: %f seconds", multiply_time);
    ConsoleLogger.log(buffer);
    file_logger.base.log(buffer);

    return 0;
}
